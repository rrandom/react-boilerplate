'use strict';

var express = require('express');
var controller = require('./controller');

var router = express.Router();

router.post('/', controller.auth);
router.get('/', controller.auth);

module.exports = router;
