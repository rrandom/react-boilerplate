'use strict';

var users = require('../../config/users').users;

exports.auth = function (req, res) {
  //console.log('req.body:', req.body);
  //console.log(req.query);
  var username = req.query.username;
  var password = req.query.password;

  for (var i = 0; i < users.length; i++) {
    if (users[i].username === username) {
      if (users[i].password === password) {
        return res.status(200).json(users);
      }
    }
  }

  return res.status(404).json({ Notfound: 404 });
}
