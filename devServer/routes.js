'use strict';

var path = require('path');

module.exports = function (app) {

  app.use('/api/auth', require('./api/auth'));

  app.route('/*')
    .get(function (req, res) {
      var viewFilePath = '404';
      var statusCode = 404;
      var result = {
        status: statusCode
      };

      res.status(result.status);
      res.render(viewFilePath, function (err) {
        if (err) {
          return res.status(result.status).json(result); }

        res.render(viewFilePath);
      });
    });
};
